from flask import Flask, make_response, request
from werkzeug.utils import secure_filename
from flask_cors import CORS
from dotenv import load_dotenv
from os import path
import os

from pydrive.auth import GoogleAuth
from pydrive.drive import GoogleDrive

gauth = GoogleAuth()
drive = GoogleDrive(gauth)

basedir = path.abspath(path.dirname(__file__))
load_dotenv(path.join(basedir, '.env'))

app = Flask(__name__)
cors = CORS(app, resources={r"/*": {"origins": "*"}})

prod = False

ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])
UPLOAD_FOLDER = 'images'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER


def allowed_file(filename):
    return '.' in filename and \
        filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

# if prod:
#     credentials = ServiceAccountCredentials.from_json_keyfile_name(
#         f"{basedir}\quiet-platform-333018-b168ef681fa9.json", scope)  # rodando localmente
# else:
#     credentials = ServiceAccountCredentials.from_json_keyfile_name(
#         'quiet-platform-333018-b168ef681fa9.json', scope)  # rodando pelo heroku


# rota inicial


@app.route('/', methods=['GET'])
def home():
    # teste para verificar se está ok
    return 'funfa'

# @app.route('/api/img/<string:img>', methods=['GET'])
# def getdashcliente(img):


@app.route('/api/setimg', methods=['POST'])
def setimgs():
    file = request.files.get('arq')
    if file.name == '':
        print('No selected file')
        return 'erro'
    if file and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))

        upload_file_list = [filename]
        for upload_file in upload_file_list:
            gfile = drive.CreateFile(
                {'parents': [{'id': '1MYj-ecq7ahHfwNW_WqKc6tUBIkkP7y-m'}]})
            # Read file and set it as the content of this instance.
            gfile.SetContentFile(upload_file)
            gfile.Upload()  # Upload the file.
            gfile = None

    return 'ok'


@app.errorhandler(404)
def not_found(error):
    return make_response({'error': 'Not found'}, 404)


@app.errorhandler(500)
def not_found(error):
    return make_response({'error': 'Internal error'}, 500)


if __name__ == "__main__":
    app.FLASK_ENV = 'production'
    app.DEBUG = False
    app.TESTING = False
    app.SECRET_KEY = 'LBMtfDCFYjD'
    app.run(host='0.0.0.0', port=8004, debug=False)
